# Language

> 在线编译各种语言，轻量级

## 使用规则


This plugin allows you to embed klipse: https://github.com/viebel/klipse
in gitbook projects.

Klipse allows code snippets in your gitbooks to be live and interactive. The code is evaluated as you type or when you press `Ctrl-Enter`.

To enable this plugin add `klipse` to your `book.json` plugins.

Now you can embed interactive code snippets (clojure, javascript, python, ruby, scheme and php) in your gitbooks.

You can read [the full interactive guide](https://book.klipse.tech/) for using klipse in a gitbook. The guide is itself a gitbook using the klipse plugin.


该插件允许您将klipse嵌入到gitbook项目中：https://github.com/viebel/klipse。

Klipse允许您的gitbook中的代码片段是实时和交互式的。

在您键入或按Ctrl-Enter时对代码进行评估。

要启用此插件，请将klipse添加到book.json插件中。

现在，您可以在gitbook中嵌入交互式代码段（clojure，javascript，python，ruby，scheme和php）。

您可以在gitbook中阅读有关使用klipse的完整交互式指南。

该指南本身就是使用klipse插件的gitbook。


### Python


```eval-python
def deduplication(self, nums):#找出排序数组的索引
    for i in range(len(nums)):
        if nums[i]==self:
            return i
    i=0
    for x in nums:
        if self>x:
            i+=1
    return i
print(deduplication(5, [1,3,5,6]))
```


### Clojure & ClojureScript

For clojure[script] code evaluation:


```eval-clojure
(map inc [1 2 3])
```


For clojurescript code transpilation:


```transpile-cljs
(map inc [1 2 3])
```

### Javascript

```eval-js
[1,2,3].map(function(x) { return x + 1;})
```


### Ruby

```eval-ruby
[1,2]*10
```

### Scheme 

```eval-scheme
(let ((x 23)
      (y 42))
  (+ x y))
```

### PHP

```eval-php
$var = ["a" => 1];
var_dump($var);
```

### Clojure & ClojureScript

```
{% klipse "eval-clojure", loopMsec="1000" %}
(rand)
{% endklipse %}
```

### Javascript

```
{% klipse "eval-js", loopMsec="1000" %}
new Date()
{% endklipse %}
```