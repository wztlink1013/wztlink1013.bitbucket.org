# Environment

<extoc></extoc>

## 一、gitbook相关
### 命令总结

```
gitbook init ：会根据SUMMARY.md初始化一系列文件夹
gitbook serve：http://localhost:4000 端口预览，和hexo预览一样
gitbook build：部署
```

```
gitbook serve --port 4008：指定端口

```

### 同步GitHub SOP

```
一般都会在GitHub上在线补充一些内容，所以同步之前一定要pull

git pull origin master
git status
git add .
git commit -m "描述信息"
git push origin master
```

### 注意

- 注意命名，目录最好不要有中文，生成的目录，最好不要改动，不然可能内部信息有稍微不兼容，会产生文件冗余